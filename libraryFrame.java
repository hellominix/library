import java.awt.FlowLayout;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;


public class libraryFrame extends JFrame implements ActionListener,ItemListener
{

	private static final long serialVersionUID = -2161726818659204433L;
    public libraryFrame()
     {
      setTitle("图书信息管理系统");
      setSize(1400,750);
     
      FlowLayout Layout = new FlowLayout();
      setLayout(Layout);
      
      add(new Label("功能选择"));
      
      funtion= new JComboBox();     
      funtion.setEditable(false);
      funtion.addItem("Insert");
      funtion.addItem("Delete");
      funtion.addItem("Query");
      funtion.addItem("Modify");
      
      add(funtion);
      funtion.addItemListener(this);
      
      add(new Label("Title"));
      titleTextField = new TextField(20);
      add(titleTextField);
      
      add(new Label("ISBN"));
      ISBNTextField = new TextField(20);
      add(ISBNTextField);
      
      add(new Label("Author"));
      authorTextField = new TextField(20);
      add(authorTextField);
      
      add(new Label("Publisher"));
      publisherTextField = new TextField(20);
      add(publisherTextField);
      
      add(new Label("Price"));
      priceTextField = new TextField(20);
      add(priceTextField);
      
      okButton = new JButton("OK");
      add(okButton);
      okButton.addActionListener(this);
      
      result = new JTextArea(50, 100);
      result.setEditable(true);
      add(result);
      
      addWindowListener(new WindowAdapter()
      {
         public void windowClosing(WindowEvent event)
         {
            try
            {
               if (con != null) con.close();
            }
            catch (SQLException e)
            {
            	
            	System.out.println(e.getMessage());
             
            }
         }
      });
   }
   
   public void itemStateChanged(ItemEvent e)
   {
		 String function = (String) funtion.getSelectedItem();
		 
		 if(function.equals("Insert"))
		 {   
			 titleTextField.setEditable(true);
			 authorTextField.setEditable(true) ;
			 ISBNTextField.setEditable(true);
			 publisherTextField.setEditable(true);
			 priceTextField.setEditable(true); 
		 } 
		 
		 if(function.equals("Query"))
		 {
			 authorTextField.setEditable(false) ;
			 ISBNTextField.setEditable(false);
			 publisherTextField.setEditable(false);
			 priceTextField.setEditable(false);
		 }
		 
		 if(function.equals("Delete"))
		 {
			 authorTextField.setEditable(false) ;
			 ISBNTextField.setEditable(false);
			 publisherTextField.setEditable(false);
			 priceTextField.setEditable(false); 
		 }
		 
		 if(function.equals("Modify"))
		 {   
			 titleTextField.setEditable(true);
			 authorTextField.setEditable(false) ;
			 ISBNTextField.setEditable(false);
			 publisherTextField.setEditable(false);
			 priceTextField.setEditable(true);
		 }
   	
   } 
   
   public void actionPerformed(ActionEvent e)
   {
		
	   if(e.getSource()==okButton)
	   {
			 String function = (String) funtion.getSelectedItem();
			 if(function.equals("Insert"))
				 insert();
			 if(function.equals("Query"))
				 query();
			 if(function.equals("Delete"))
				 delete();
			 if(function.equals("Modify"))
				 modify();         
		}
   }
   
 

   
   private boolean insert()
   {
	   
	  title = titleTextField.getText();
	  author = authorTextField.getText();
	  ISBN = ISBNTextField.getText();
	  publisher = publisherTextField.getText();
	  price = priceTextField.getText();
	 
	 if(title == null || title.isEmpty())
		 {
		  JOptionPane.showMessageDialog(this, "请输入完整记录");  
		  return false;
		 }
	 
	 if(ISBN == null || ISBN.isEmpty())
	 {
	  
	  JOptionPane.showMessageDialog(this, "请输入完整记录");  
	  return false;
	 }
	 
	 if(author == null || author.isEmpty())
	 {
	  
	  JOptionPane.showMessageDialog(this, "请输入完整记录");  
	  return false;
	 }

	 if(publisher == null ||publisher.isEmpty())
	 {
	  
	  JOptionPane.showMessageDialog(this, "请输入完整记录");  
	  return false;
	 }
	 
	 if(price == null || price.isEmpty())
	 {
	  
	  JOptionPane.showMessageDialog(this, "请输入完整记录");  
	  return false;
	 }

	 
	 
	 
		try 
		{  
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");  
        }
        catch(ClassNotFoundException e)
        {  
	      System.out.println(""+e.getMessage());
        }
		
       try 
       {  
    	   
    	   con=DriverManager.getConnection("jdbc:odbc:tushu");
    	   
    	   Statement sql=con.createStatement();
    	   
    	   String insert="insert into books(title,ISBN,author,publisher,price) values"; 
    	  
    	   String recode="("+"'"+title+"'"+","+"'"+ISBN+"'"+","+"'"+author+"'"+","+"'"+publisher+"'"+","+"'"+price+"'"+")";

			
		   sql.executeUpdate(insert+recode);
		   
		   result.setText(null);
           result.setText("你插入的是：");
           result.append("\n");
           result.append("书名："+title+"   序列号："+ISBN+"  作者："+author+"  出版社： "+publisher+"   价格： "+price);
           result.append("\n");
           result.append("插入后图书信息为：");
           result.append("\n");
    	 
    	   ResultSet  rs=sql.executeQuery("SELECT * FROM books");
           while(rs.next())
           {
        	
             title=rs.getString(1);  
             ISBN=rs.getString(2);
             author=rs.getString(3);
        
             publisher=rs.getString(4);
             price=rs.getString(5);
              	
             result.append("书名："+title+"  序列号："+ISBN+"  作者："+author+"  出版社： "+publisher+"  价格： "+price);
             result.append("\n");
           }
                   
           rs.close();
           sql.close();
           con.close();
          
          }
           
           catch(SQLException e) 
           {  
        	   System.out.println(e);
           	  JOptionPane.showMessageDialog(this, "你输入的不正确:"+e.getMessage());
           }
       return true;
       
} 

  
   
   private void query()
   {
		    title = titleTextField.getText();

	 
			try 
			{  
				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");  
	        }
	        catch(ClassNotFoundException eee)
	        {  
		      System.out.println(""+eee);
	        }
			
	       try {  
	    	   
	    	   con=DriverManager.getConnection("jdbc:odbc:tushu");
	    	   
	    	   Statement sql=con.createStatement();
	    	   
	    	   String command;
	    	   if(title == null||title.isEmpty())
	    		    command = "SELECT * FROM books";
	    	   else
	    		    command = "SELECT * FROM books WHERE title='"+title+"'";
	    	   
	    	   ResultSet  rs=sql.executeQuery(command);
	    	   
	    	   result.setText(null);
	    	   result.append("查询结果为：");
	    	   result.append("\n");
	    	   
	           while(rs.next())
	           {
	        	
	               title=rs.getString(1);  
	               ISBN=rs.getString(2);
	               author=rs.getString(3);
	          
	               publisher=rs.getString(4);
	               price=rs.getString(5);
	                	
	               result.append("书名："+title+"  序列号："+ISBN+"  作者："+author+"  出版社： "+publisher+"  价格： "+price);
	               result.append("\n");
	           }
	           rs.close();
	           sql.close();
	           con.close();
	          }
	           catch(SQLException e) 
	           {  
	        	    System.out.println(e.getMessage());
	           		JOptionPane.showMessageDialog(this, "你输入的不正确: "+e.getMessage());
	           }   
   }
   
   private void delete()
   {
		
	       title = titleTextField.getText();
		   
			try 
			{  
				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");  
	        }
	        catch(ClassNotFoundException e)
	        {  
		      System.out.println(""+e.getMessage());
	        }
			
	       try {  
	    	   
	    	   con=DriverManager.getConnection("jdbc:odbc:tushu");
	    	   
	           Statement sql=con.createStatement();
	    	
	    	   String command = "SELECT * FROM books WHERE title='"+title+"'";
	    	   ResultSet  rs=sql.executeQuery(command);
	    	   
	    	   result.setText(null);
	           result.setText("你删除了:");
	           result.append("\n");
	           while(rs.next())
	           {
	        	
	              title=rs.getString(1);  
	              ISBN=rs.getString(2);
	              author=rs.getString(3);
	              publisher=rs.getString(4);
	              price=rs.getString(5);
	              
	              result.append("书名："+title+"  序列号："+ISBN+"  作者："+author+"  出版社： "+publisher+"  价格： "+price);
	              result.append("\n");
	           }
	           
	           rs.close();
	           String delete = "delete from books where title='"+title+"'";
	           sql.executeUpdate(delete);
	           
	           sql.close();
	           con.close();
	          }
	           catch(SQLException e) 
	           {  
	        	    System.out.println(e.getMessage());
	           		JOptionPane.showMessageDialog(this, "你输入的不正确: "+e.getMessage());
	           }   
   }
   
     private void modify()
      {
		   title = titleTextField.getText();

		   price = priceTextField.getText();
	
			try 
			{  
				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");  
	        }
	        catch(ClassNotFoundException e)
	        {  
		      System.out.println(""+e.getMessage());
	        }
			
	       try {  
	    	   
	    	   con=DriverManager.getConnection("jdbc:odbc:tushu");
	    	   
	           Statement sql=con.createStatement();
	           
	           String modify="UPDATE books SET price='"+price+"'  WHERE title='"+title+"'";
	           sql.executeUpdate(modify);
	           
	           result.setText(null);
	           result.setText("你修改了:");
	           result.append("\n");
	           
	    	   String command = "SELECT * FROM books WHERE title='"+title+"'";
		       ResultSet  rs=sql.executeQuery(command);
	           while(rs.next())
	           {
	        	
	              title=rs.getString(1);  
	              ISBN=rs.getString(2);
	              author=rs.getString(3);
	              publisher=rs.getString(4);
	              price=rs.getString(5);
	                	
	              result.append("书名："+title+"  序列号："+ISBN+"  作者："+author+"  出版社： "+publisher+"  价格： "+price);
	              result.append("\n");
	           }
	           rs.close();
	           sql.close();
	           con.close();
	          }
	           catch(SQLException e) 
	           {  
	        	    System.out.println(e.getMessage());
	           		JOptionPane.showMessageDialog(this, "你的输入不正确: "+e.getMessage());
	           }  
     }
   
   
		   
		


   private JComboBox funtion;
   
   private TextField titleTextField;
   private TextField ISBNTextField;
   private TextField authorTextField;
   private TextField publisherTextField;
   private TextField priceTextField;
   private JTextArea result;
   
   private String title;
   private String author;
   private String ISBN;
   private String publisher;
   private String price;
   
   private JButton okButton;
   private Connection con;
 
   
}

